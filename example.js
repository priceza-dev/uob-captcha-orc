const { detectTextFromImageFile } = require('./utils/ocr');
const fs = require('fs');
const PromptList = require('prompt-list');

const getTextFromImage = async (imagename) => {
    const text = await detectTextFromImageFile(`${__dirname}/resource/${imagename}`);
    console.log(`Detect Result: ${text}`);
}

const main = async () => {
    const imageList = fs.readdirSync(`${__dirname}/resource`);
    var prompt = new PromptList({
        name: 'image',
        message: 'Which image would you like to get text?',
        choices: imageList
    })

    prompt.ask((answer) => {
        getTextFromImage(answer);
    });
}

main();