# To use Google Cloud Vision API

## Require binding billing account to the project
- Go to https://console.cloud.google.com/billing
- Create billing account
- After create billing account, go to https://console.cloud.google.com/
- Select your project, go to hamburger menu, then select billing menu
- Link billing account

## Require Cloud Vision API to be enabled
You could enable Cloud Vision API @ https://console.developers.google.com/apis/library/vision.googleapis.com


## Require GCP Service Account Key
You could create your **Service Account Key** @ https://console.cloud.google.com/apis/credentials

---

## Running app with Cloud Vision API
You need to set **GOOGLE_APPLICATION_CREDENTIALS** environment variable to be  your Service Account Key file path, since vision client will authenticate using this file - https://cloud.google.com/docs/authentication/getting-started

For example,
```
$ GOOGLE_APPLICATION_CREDENTIALS="/path/to/your-service-accont-key.json" node app.js
```

---

## Example
Get your GCP Service Account Key and place it into this folder - naming as **service-account-key.json**

Then try **app.js**, by running
```
$ npm run text-detect-test
```