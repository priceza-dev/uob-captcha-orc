const fs = require('fs');
const vision = require('@google-cloud/vision');
const client = new vision.ImageAnnotatorClient();

const detectTextFromImageFile = async (imgPath) => {
    const [result] = await client.textDetection(imgPath);

    if (result && result.fullTextAnnotation && result.fullTextAnnotation.text) {
        return result.fullTextAnnotation.text;
    }
    return;
}

const detectTextFromImageUrl = async (url) => {
    const request = {
        image: { 
            'source': {
                'imageUri': 'http://oi66.tinypic.com/1ftr43.jpg'
            }
        },
            features: [{type: 'TEXT_DETECTION'}
        ]
    };

    const [result] = await client.annotateImage(request)

    if (result && result.fullTextAnnotation && result.fullTextAnnotation.text) {
        return result.fullTextAnnotation.text;
    }
    return;
}

module.exports = {
    detectTextFromImageFile,
    detectTextFromImageUrl
}
